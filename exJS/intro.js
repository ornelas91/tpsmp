console.log("Hello world!");
let variavel = -5;
console.log("variavel " + variavel + " é do tipo: " +typeof(variavel));

//operações matemáticas
let numero1 = 5;
let numero2 = 10;

console.log("" + numero1+""+""+numero2);

console.log(5*2);

try{
    let idade = 40;

    if(idade === undefined){
        console.log("Idade não foi criada! ");
    }else{
        console.log("sua idade é " + idade);
    }
}catch(error){
    console.log("Erro na variavel idade! ");
}

function mostrarIdade(){
    let idade = 41;
    if(idade < 42){
        console.log("jovem");
    }else{
        console.log("Não muito jovem");
    }
}
mostrarIdade();

function mostrarIdade2(vlIdade){
    if(vlIdade < 42){
        console.log(vlIdade + " - jovem");
    }else{
        console.log(vlIdade + " - Não muito jovem");
    }
}

mostrarIdade2(42);
mostrarIdade2(40);

const mostraIdade3 = (vlIdade) => {
    if(vlIdade < 42){
        return vlIdade + " - jovem";
    }else{
        return vlIdade + " - Não muito jovem";
    }
}

console.log(mostraIdade3(42));